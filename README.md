My Netdata config for monitoring my infrastructure.

Just clone this repo to ```/etc/netdata/``` of the netdata host and restart netdata.

```
systemctl restart netdata
```
